//
//  ViewController.m
//  CoreLocationDemo
//
//  Created by James Cash on 17-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
@import CoreLocation;
@import MapKit;
#import "WhateverAnnotation.h"

@interface ViewController () <CLLocationManagerDelegate, MKMapViewDelegate>
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic,strong) CLLocationManager *locMgr;
@property (nonatomic,strong) CLGeocoder *coder;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.locMgr = [[CLLocationManager alloc] init];
    self.locMgr.desiredAccuracy = kCLLocationAccuracyKilometer;
    self.locMgr.distanceFilter = kCLDistanceFilterNone;
    self.locMgr.delegate = self;

    [self.locMgr requestWhenInUseAuthorization];

    [self.locMgr startUpdatingLocation];

    self.mapView.delegate = self;
    [self.mapView setShowsUserLocation:YES];
    self.mapView.mapType = MKMapTypeMutedStandard;

    WhateverAnnotation *anno = [[WhateverAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(43.6447391, -79.394702) colour:UIColor.cyanColor];
    [self.mapView addAnnotation:anno];

    [self.mapView registerClass:[MKPinAnnotationView class] forAnnotationViewWithReuseIdentifier:@"pinAnnotation"];


    self.coder = [[CLGeocoder alloc] init];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    NSLog(@"Authorization status changed: %d", status);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    NSLog(@"Moved to %@", locations);
//    [self.mapView setCenterCoordinate:locations[0].coordinate
//                             animated:YES];
    MKCoordinateRegion region = MKCoordinateRegionMake(locations[0].coordinate,
                           MKCoordinateSpanMake(0.1, 0.1));
    [self.mapView setRegion:region animated:YES];

    [self.coder reverseGeocodeLocation:locations[0] completionHandler:^(NSArray<CLPlacemark *> * placemarks, NSError *  error) {
        if (error != nil) {
            NSLog(@"Error reverse geocoding: %@", error.localizedDescription);
            return;
        }

        NSLog(@"Location: %@", placemarks);
    }];
}

#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[WhateverAnnotation class]]) {
        MKPinAnnotationView *pin = [mapView dequeueReusableAnnotationViewWithIdentifier:@"pinAnnotation"];

        WhateverAnnotation *anno = annotation;
        pin.pinTintColor = anno.displayColour;

        return pin;
    }
    return nil;
}

@end
