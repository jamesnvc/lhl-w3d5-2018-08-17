//
//  WhateverAnnotation.m
//  CoreLocationDemo
//
//  Created by James Cash on 17-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "WhateverAnnotation.h"

@implementation WhateverAnnotation

- (instancetype)initWithCoordinate:(CLLocationCoordinate2D)coordinate colour:(UIColor *)colour
{
    self = [super init];
    if (self) {
        _coordinate = coordinate;
        _displayColour = colour;
    }
    return self;
}

@end
