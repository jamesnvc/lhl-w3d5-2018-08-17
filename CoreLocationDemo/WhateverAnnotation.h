//
//  WhateverAnnotation.h
//  CoreLocationDemo
//
//  Created by James Cash on 17-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreLocation;
//#import <CoreLocation/CoreLocation.h>
@import MapKit;

@interface WhateverAnnotation : NSObject <MKAnnotation>

@property (nonatomic,assign) CLLocationCoordinate2D coordinate;
@property (nonatomic,strong) UIColor *displayColour;

- (instancetype)initWithCoordinate:(CLLocationCoordinate2D)coordinate colour:(UIColor*)colour;

@end
